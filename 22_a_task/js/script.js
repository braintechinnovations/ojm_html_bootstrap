/*
    * Creare un sistema di controllo degli ingressi al ristorante:
    * In input avremo la temperatura (salvata in una variabile).
    * Se la temperatura è maggiore o uguale di 37.5° vietare l'ingresso al ristorante, se la temperatura
    * è inferiore a 37.5° permettere l'ingresso.
    *
    * Attenzione!!!!!!!! Non ci vuole un medico per dire che al di sotto dei 34° mostrare un errore,
    * al di sopra dei 42° mostrare lo stesso errore!
*/

let temperatura = 8.1;

// SOLUZIONE CON OR
// if(temperatura < 34 || temperatura > 42){
//     console.log("Errore, non siamo a Racoon city");
// }
// else{
//     if(temperatura >= 37.5){
//         console.log("Non puoi entrare") 
//     }
//     else{
//         console.log("Benvenuto!")
//     }
// }

// SOLUZIONE CON AND
if(temperatura >= 34 && temperatura <= 42){
    if(temperatura >= 37.5){
        console.log("Non puoi entrare") 
    }
    else{
        console.log("Benvenuto!")
    }
}
else{
    console.log("Errore, non siamo a Racoon city");
}


