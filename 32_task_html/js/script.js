function inserisciLibro(){
    document.getElementById("box-errore").classList.add("nascosto")

    let varTitolo = document.getElementById("input-titolo").value;
    let varAutore = document.getElementById("input-autore").value;

    if(varTitolo.length != 0 && varAutore.length != 0){
        elencoLibri += `<li class="list-group-item">${varTitolo} - ${varAutore}</li>`;
        document.getElementById("elenco-libri").innerHTML =  elencoLibri;

        document.getElementById("input-titolo").value = "";
        document.getElementById("input-autore").value = "";
        
        document.getElementById("input-titolo").focus();
    }
    else{
        document.getElementById("box-errore").classList.remove("nascosto")
    }
}

let elencoLibri = "";