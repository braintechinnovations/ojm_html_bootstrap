/*
    Nella "Guida Michelin" c'è la necessità di tracciare ogni locale presente nel territorio mondiale, vi chiedo di strutturare un piccolo software con HTML/CSS/JavaScript e localStorage che:

    1. FACILE - Sia composto da due pagine e permetta di navigare tra esse (utilizzare una delle navbar di Bootstrap sarebbe carino)
    2. FACILE - Una delle pagine sarà dedicata all'inserimento di un locale caratterizzato da: Nome del locale, Telefono, Indirizzo email, Via, CIttà, Provincia, CAP, Nazione, Partita IVA, Latitudine e Longitudine.
    3. FACILE - Per ogni locale assegnare in automatico un identificativo numerico di 8 cifre (cercate i numeri Random di Javascript)
    4. INTERMEDIO - Visualizzare tutti gli inserimenti all'interno di una tabella
    5. INTERMEDIO - Permettere l'eliminazione tramite tabella con un pulsante per ogni riga
    6. DIFFICILE - Modifica di un locale tramite modale

    7. CHALLENGE - Se fosse introdotta la gestione dinamica delle categorie? Creare una pagina (single page in questo caso) che si occupi del salvataggio in LocalStorage delle categorie che saranno visualizzate dinamicamente su una SelectBox all'interno del form di inserimento del locale. (Posso scegliere se il locale è di alta cucina/pub/agriturismo, ecc...
    8. CHALLENGE - Se vi chiedessi di creare (sopra alla tabella dell'elenco locali con un altro piccolo form) una ricerca che filtri i risultati per categoria?
*/

/**
 * 
 * @param {*} nodoInput 
 * @returns true in caso di esito positivo | false se il dato non è valido
 */
function verificaDato(nodoInput){
    if(nodoInput.value.trim().length == 0){
        alert("Attenzione il valore non può essere vuoto!");
        nodoInput.focus();
        return false;
    }
    else{
        return true;
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function nuovoId(){
    idAutoincrementale = idAutoincrementale + 1;
    localStorage.setItem("ai", idAutoincrementale);
    return idAutoincrementale;
}

function inserisciLocale(){
    let inpCate = document.getElementById("in-cate");
    let inpNome = document.getElementById("in-nome");
    let inpTele = document.getElementById("in-tele");
    let inpEmai = document.getElementById("in-emai");
    let inpPiva = document.getElementById("in-piva");
    let inpViac = document.getElementById("in-viac");
    let inpCitt = document.getElementById("in-citt");
    let inpProv = document.getElementById("in-prov");
    let inpCapc = document.getElementById("in-capc");
    let inpLati = document.getElementById("in-lati");
    let inpLong = document.getElementById("in-long");

    if(!verificaDato(inpCate)) return;
    if(!verificaDato(inpNome)) return;
    if(!verificaDato(inpTele)) return;
    if(!verificaDato(inpEmai)) return;
    if(!verificaDato(inpPiva)) return;
    if(!verificaDato(inpViac)) return;
    if(!verificaDato(inpCitt)) return;
    if(!verificaDato(inpProv)) return;
    if(!verificaDato(inpCapc)) return;
    if(!verificaDato(inpLati)) return;
    if(!verificaDato(inpLong)) return;

    let locale = {
        cate: inpCate.value,
        nome: inpNome.value,
        id: nuovoId(),          //Possibile randomico con getRandomInt(1, 9999)
        codi: getRandomInt(10000000, 99999999),
        tele: inpTele.value,
        emai: inpEmai.value,
        piva: inpPiva.value,
        viac: inpViac.value,
        citt: inpCitt.value,
        prov: inpProv.value,
        capc: inpCapc.value,
        lati: inpLati.value,
        long: inpLong.value,
    }

    elencoLocali.push(locale);                                      //Aggiungo il locale all'array
    localStorage.setItem("locali", JSON.stringify(elencoLocali))    //Salvo nel LS il nuovo elenco dei locali stringhificato
    alert("Inserimento effettuato con successo");

    inpNome.value =  inpTele.value =  inpEmai.value =  inpPiva.value =  inpViac.value =  inpCitt.value =  inpCapc.value = inpLati.value = inpLong.value = inpProv.value = "";

}

function inserisciCategoria(){
    let inpNome = document.getElementById("in-nome");

    if(!verificaDato(inpNome)) return;

    for(let [idx, obj] of elencoCategorie.entries()){
        if(inpNome.value == obj.nome){
            alert("Categoria già presente!");
            return;
        }
    }

    let categoria = {
        nome: inpNome.value
    }

    elencoCategorie.push(categoria);
    localStorage.setItem("categorie", JSON.stringify(elencoCategorie));

    alert("Inserimento effettuato con successo");

    inpNome.value = "";
}

function pulisciRicerca(){
    let queryRicerca = document.getElementById("in-cate");
    queryRicerca.value = "";
    queryRicerca.focus();

    filtroCategorie();
}

function stampaElenco(elenco){
    for(let [idx, obj] of elenco.entries()){
        let tempTr = document.createElement("tr");

        let tempTdNome = document.createElement("td");
        tempTdNome.innerText = obj.nome;
        tempTr.appendChild(tempTdNome);

        let tempTdTelefono = document.createElement("td");
        tempTdTelefono.innerText = obj.tele;
        tempTr.appendChild(tempTdTelefono);

        let tempTdEmail = document.createElement("td");
        tempTdEmail.innerText = obj.emai;
        tempTr.appendChild(tempTdEmail);

        let tempTdCategoria = document.createElement("td");
        tempTdCategoria.innerText = obj.cate;
        tempTr.appendChild(tempTdCategoria);

        document.getElementById("elenco-locali").appendChild(tempTr);
    }
}

function filtroCategorie(){
    let queryRicerca = document.getElementById("in-cate");
    // if(!verificaDato(queryRicerca)) return;

    let elencoRisultati = [];

    if(queryRicerca.value.trim().length == 0){
        elencoRisultati = elencoLocali;
    }
    else{
       for(let [idx, obj] of elencoLocali.entries()){
            if(queryRicerca.value.toUpperCase() === obj.cate.toUpperCase()){
                elencoRisultati.push(obj)
            }
        } 
    }
    

    console.log(elencoRisultati);
    document.getElementById("elenco-locali").innerHTML = "";

    stampaElenco(elencoRisultati);
}

//Elenco locali, inizializzazione
let elencoLocali;
if(localStorage.getItem("locali") != null)
    elencoLocali = JSON.parse(localStorage.getItem("locali"));
else
    elencoLocali = [];

let idAutoincrementale;
if((localStorage.getItem("ai") != null))
    idAutoincrementale = Number.parseInt(localStorage.getItem("ai"));
else
    idAutoincrementale = 0;

//Categorie, Inizializzazione
let elencoCategorie;
if(localStorage.getItem("categorie") != null)
    elencoCategorie = JSON.parse(localStorage.getItem("categorie"))
else
    elencoCategorie = [];

//Appendo le categorie alla pagina di inserimento locale
// var nuovaOption = document.createElement("option");
// nuovaOption.value = "PUB";
// nuovaOption.innerText = "Pub";

// var nuovaOptionDUE = document.createElement("option");
// nuovaOptionDUE.value = "RISTORANTE";
// nuovaOptionDUE.innerText = "Ristorante";

// document.getElementById("in-cate").appendChild(nuovaOption);
// document.getElementById("in-cate").appendChild(nuovaOption);
// document.getElementById("in-cate").appendChild(nuovaOption);
// document.getElementById("in-cate").appendChild(nuovaOption);

// SOLUZIONE CON CREATE ELEMENT
// let selectDaPopolare =  document.getElementById("in-cate");
// for(let [idx, obj] of elencoCategorie.entries()){
//     let nuovaOption = document.createElement("option");         //Creo il tag vuoto!
//     nuovaOption.value = obj.nome;
//     nuovaOption.innerText = obj.nome;
//     selectDaPopolare.appendChild(nuovaOption);
// }

// SOLUZIONE CON CONCATENAZIONE DI STRINGHE

// console.log("PRIMA")
if(document.getElementById("in-cate") != null){
    let stringaSelect = "";
    for(let [idx, obj] of elencoCategorie.entries()){
        stringaSelect += `<option value="${obj.nome}">${obj.nome}</option>`
    }
    
    document.getElementById("in-cate").innerHTML = stringaSelect;
}
// console.log("DOPO")

if(document.getElementById("tbl-categorie") != null){
    
    for(let [idx, obj] of elencoCategorie.entries()){

        let trTemp = document.createElement("tr");
        let tdTempValore = document.createElement("td");
        tdTempValore.innerText = obj.nome;
        trTemp.appendChild(tdTempValore)

        let tdAzioni = document.createElement("td");
        trTemp.appendChild(tdAzioni);
        let button = document.createElement("button");
        button.className = "btn btn-primary"
        button.innerText = "Elimina"
        tdAzioni.appendChild(button)

        document.getElementById("tbl-categorie").appendChild(trTemp);
    }

}

if(document.getElementById("elenco-locali") != null){
    stampaElenco(elencoLocali)
}