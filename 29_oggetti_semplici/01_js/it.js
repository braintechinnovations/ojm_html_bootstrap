const menu_iniziale =   "Effettua la scelta:\n" +
                        "1. Inserire un libro\n" +
                        "2. Eliminare un libro tramite il suo ISBN\n" +
                        "3. Stampare l'elenco dei libri\n" +
                        "4. Uscire dal programma\n";

const ins_titolo = "Inserisci il titolo per favore";
const ins_autore = "Inserisci l'autore per favore";
const ins_isbn = "Inserisci il codice ISBN del libro"

const not_found = "Comando non riconosciuto!"
const insert_ok = "Inserimento effettuato con successo"