// Oggetto letterale

// let personaUno = {
//     nome: "Giovanni",
//     cognome: "Pace",
//     eta: 35
// }

// let personaDue = {
//     eta: 32,
//     nome: "Marika",
//     cognome: "Viola"
// }


// // console.log(personaUno, personaDue);

// // console.log(personaUno.cognome)
// // console.log(personaUno['cognome'])

// personaDue.capelli = "Castani";

// console.log(personaDue)

// ------------------------------------------------

// let personaUno = {
//     nome: "Giovanni",
//     cognome: "Pace",
//     eta: 35
// }

// let personaDue = {
//     eta: 32,
//     nome: "Marika",
//     cognome: "Viola"
// }

// let rubrica = [personaUno, personaDue];

// for(let i=0; i<rubrica.length; i++){
//     console.log(rubrica[i].eta)
// }

// let rubrica = [
//     {
//         nome: "Giovanni",
//         cognome: "Pace",
//         animali: ["Gatto", "Criceto"],
//         indirizzo: {
//             via: "Via le mani dal naso",
//             citta: "L'Aquila",
//             provincia: "AQ",
//             cap: 67100
//         }
//     },
//     {
//         nome: "Marika",
//         cognome: "Viola",
//         eta: 33
//     }
// ]

// console.log(rubrica[0])

// let giovanni = {
//     "nome": "Giovanni",
//     "cognome": "Pace",
//     "elenco carte": [
//         {
//             "numeroCarta": 123456,
//             "negozio": "Conad",
//             "data": "2020-01-01"
//         },
//         {
//             "numeroCarta": 123457,
//             "negozio": "Coop",
//             "data": "2020-01-02"
//         },
//     ]
// }

// console.log(giovanni);

// delete giovanni.cognome;                                //Eliminazione di una proprietà
// console.log(giovanni)

// console.log(giovanni.elencoCarte[1].numeroCarta);

// -------------------------------------------------------
/**
 * Creare un sistema di gestione di libri:
 * ogni libro sarà caratterizzato da: Titolo, Autore, ISBN
 * 
 * Nel menu di interazione avrò la possibilità di:
 * 1. Inserire un libro                             <<
 * 2. Eliminare un libro tramite il suo ISBN        <<
 * 3. Stampare l'elenco dei libri                   <<
 * 4. Uscire dal programma
 */

// let insAbi = true;
// let scaffale = [
//     {
//         titolo: "La tempesta",
//         autore: "W.S.",
//         isbn: 123456
//     },
//     {
//         titolo: "Lo Hobbit",
//         autore: "JRRT",
//         isbn: 123457
//     },
//     {
//         titolo: "Il codice Da Vinci",
//         autore: "DB",
//         isbn: 123458
//     },
// ];

// while(insAbi){
//     let input = prompt(menu_iniziale)

//     switch(input){
//         case "1":                               //INSERIMENTO
//             let varTitolo = prompt(ins_titolo);
//             let varAutore = prompt(ins_autore);
//             let varIsbn = prompt(ins_isbn)

//             let libro = {
//                 titolo: varTitolo,
//                 autore: varAutore,
//                 isbn: varIsbn
//             }

//             scaffale.push(libro);
//             alert(insert_ok);
//             break;
//         case "2":                               //ELIMINAZIONE
//             let isbnEliminazione = prompt(ins_isbn);

//             for(let i=0; i<scaffale.length; i++){
//                 if(scaffale[i].isbn === Number.parseInt(isbnEliminazione)){
//                     scaffale.splice(i, 1);
//                     alert("Libro eliminato con successo!")
//                 }
//             }
//             break;
//         case "3":                               //STAMPA
//             for(let i=0; i<scaffale.length; i++){
//                 console.log(  scaffale[i].autore + " - " + scaffale[i].titolo + " - " + scaffale[i].isbn )
//             }
//             break;
//         case "4":                               //QUIT
//             insAbi = !insAbi;
//             break;
//         default:
//             alert(not_found)
//     }
// }

// -----------------------------------------------------------

// let elenco = ["Biscotti", "Latte", "Caffè", "Pane"];

// for(let i=0; i<elenco.length; i++){
//     console.log(elenco[i])
// }

// for(let elemento of elenco){
//     console.log(elemento)
// }

// console.log("-----OF------")

// for(let [elemento, indice] of elenco.entries()){
//     console.log(elemento, indice)
// }

// let scaffale = [
//     {
//         titolo: "La tempesta",
//         autore: "W.S.",
//         isbn: 123456
//     },
//     {
//         titolo: "Lo Hobbit",
//         autore: "JRRT",
//         isbn: 123457
//     },
//     {
//         titolo: "Il codice Da Vinci",
//         autore: "DB",
//         isbn: 123458
//     },
// ];

// for(let [indice, elemento] of scaffale.entries()){
//     console.log(indice, elemento)
// }

// ------------------- SCANSIONE OGGETTO -----------------------

// let libro = {
//     titolo: "Lo Hobbit",
//     autore: "JRRT",
//     isbn: 123457
// };

// for(let [chiave, valore] of Object.entries(libro)){
//     console.log(chiave);
//     console.log(valore)
// }

// -------------------------------------------------------------
// Scansione oggetti eterogenei:

// let contenitore = ["EL1", "EL2", {nome: "GIovanni", cognome: "Pace"}]

// for(let elemento of contenitore){
//     console.log(elemento)
// }

// console.log("-----IN------")

// let contenitore = ["EL1", "EL2", {nome: "GIovanni", cognome: "Pace"}]

// for(let indice in contenitore){
//     console.log(contenitore[indice]);
// }

// ------------------------------------------------------------
// Shift e Pop

let elenco = new Array();
elenco.push("Giovanni");
elenco.push("Mario");
elenco.push("Veronica");
elenco.push("Mirko");

console.log(elenco)

// elenco.pop()                     //LIFO
elenco.shift();                     //FIFO

console.log("----------------")
console.log(elenco)


