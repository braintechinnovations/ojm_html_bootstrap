// Array semplici
// Tipologia disomogenea di dato all'interno dello stesso array
// let contenitore = ["Fiat", "Maserati", 2, "Ferrari"];

// console.log(typeof contenitore[0]);
// console.log(typeof contenitore[1]);
// console.log(typeof contenitore[2]);
// console.log(typeof contenitore[3]);

// ---------------------------------------------------
//                  0           1           2              3      4
// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// console.log(contenitore.length);

// for(let i=0; i<contenitore.length; i++){
//     console.log( contenitore[i] )
// }

// for(let i=1; i<=contenitore.length; i++){
//     console.log( contenitore[i-1] )
// }

// ---------------------------------------------------

//TASK: Creare un piccolo programma che prende in input il nome di 
//un'automobile e restituisce se questa c'è o no in elenco!

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];


// let ins_abi = true;
// while(ins_abi){

//     let input = prompt("Che macchina cerchi? Per uscire digita Q");

//     if(input == "Q")
//         ins_abi = !ins_abi
//     else{
        
//         let trovato = false;
//         for(let i=0; i<contenitore.length; i++){
//             if(contenitore[i] == input){
//                 trovato = true;
//             }
//         }

//         if(trovato){
//             alert("Trovato! ;)")
//         }
//         else{
//             alert("Non trovato! ;(")
//         }

//     }

// }

// ----------------------------------------------------
// Trasformazione dell'array in una stringa separata da ","

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// let stringa = "";
// for(let i=0; i<contenitore.length; i++){
//     stringa += contenitore[i];

//     if(i < (contenitore.length - 1))
//         stringa += ","
// }

// console.log(stringa);

// //EQUIVALENTE:
// console.log( contenitore.join(",") );

// ----------------------------------------------------
//Mandare in output solo le automobili presenti agli indici dispari...

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// let stringa = "";
// for(let i=0; i<contenitore.length; i = i + 2){
//     stringa += contenitore[i];

//     if(i < (contenitore.length - 1))
//         stringa += ","
// }

// console.log(stringa);

//Oppure

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// let stringa = "";
// for(let i=0; i<contenitore.length; i++){
    
//     if((i % 2) == 0){
//         stringa += contenitore[i];

//         if(i < (contenitore.length - 1))
//         stringa += ","
//     }

// }

// console.log(stringa);

// ---------------------------------------------------------

// let stringa = "pane,marmellata,cerali,latte,frutta";

// // console.log(stringa.length);            //Lunghezza di una stringa (proto forma di Array)

// let arrayStringa = stringa.split(",");
// console.log(arrayStringa);
// console.table(arrayStringa);

// --------------------------------------------------------

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// // console.log( contenitore[2] )

// contenitore[2] = "Mercedes";
// contenitore[675] = "Lexus";


// for(let i=0; i<contenitore.length; i++){
//     console.log(contenitore[i])                 //Errore negli indici, viene printato 670 volte "undefined"
// }

// -----------------------------------------------------------

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];

// contenitore.push("Lexus");
// console.log(contenitore)

// contenitore[contenitore.length + 1] = "Audi";
// console.log(contenitore[6])
// console.log(contenitore)

// -----------------------------------------------------------
// Creare un elenco di prodotti in input (con prompt) e poi, 
// al termine concatenarli in un'unica stringa separata da virgola (CSV)

// let insAbilitato = true;
// let arrayContenitore = [];

// while(insAbilitato){
//     let input = prompt("Inserisci il valore o digita Q per terminare l'inserimento e stampare la stringa.")

//     if(input == "Q")
//         insAbilitato = !insAbilitato;
//     else{
//         arrayContenitore.push(input);
//     }
// }

// console.log(arrayContenitore.join(","));

// -----------------------------------------------------------
// Sottoinsieme di un array

// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];
// console.log(contenitore)

// let secondario = contenitore.slice(2, 4);
// console.log(secondario);

// -----------------------------------------------------------
// Eliminazione di uno o più elementi
// let contenitore = ["Fiat", "Maserati", "Lamborghini", "Ferrari", "BMW"];
// contenitore.splice(1, 2);                                                   //Elimina due elementi dalla posiziopne 1
// contenitore.splice(1, 2, "Audi", "Lexus");                                  //Sostituisce i due elementi dalla posizione 1
// contenitore.splice(1, 0, "Audi", "Lexus");                                  //Aggiunge due elementi alla posizione 1

// console.log(contenitore)

// -----------------------------------------------------------
// let persona = ["Giovanni", "Pace", 34, "Abruzzo", 1.74];

// let rubrica = [
//     ["Giovanni", "Pace", 34, "Abruzzo", 1.74],
//     ["Mario", "Pace", 32, "Abruzzo", 1.72],
//     ["Valeria", "Verdi", 33, "Lazio", 1.75]
// ]

// console.table(rubrica);

// console.log("-------------------");
// console.log(rubrica[1][2])

// ---------------------------------------------------------
// Array N Dimensionale (3)
// let rubrica = [
//     ["Giovanni", "Pace", 34, "Abruzzo", 1.74, ["Via le mani dal naso", 89, "Roma"]],
//     ["Mario", "Pace", 32, "Abruzzo", 1.72, ["Piazza la bomba e scappa", "snc", "L'Aqula"]],
//     ["Valeria", "Verdi", 33, "Lazio", 1.75, ["Nessun indirizzo"]]
// ]

// console.table(rubrica)

// ---------------------------------------------------------
// let rubrica = [
//     ["Giovanni", "Pace", 34, "Abruzzo", 1.74],
//     ["Mario", "Pace", 32, "Abruzzo", 1.72],
//     ["Valeria", "Verdi", 33, "Lazio", 1.75]
// ]

// debugger;
// for(let i=0; i<rubrica.length; i++){
    
//     for(let j=0; j<rubrica[i].length; j++){
//         console.log( rubrica[i][j] );
//     }

// }

// ---------------------------------------------------------
// Calcolare il valore medio dell'età presente nell'Array:

// let rubrica = [
//     ["Giovanni", "Pace", 34, "Abruzzo", 1.74],
//     ["Mario", "Pace", 32, "Abruzzo", 1.72],
//     ["Valeria", "Verdi", 33, "Lazio", 1.75]
// ]

// let somma = 0;
// for(let i=0; i<rubrica.length; i++){
//     // console.log( rubrica[i][2] )
//     somma += rubrica[i][2]
// }

// let media = somma / rubrica.length;
// console.log(`L'età media della rubrica è: ${media}`);

// ---------------------------------------------------------
/**
 * Scrivere un software che:
 * a. Visualizza un menu così composto: 
 *          1. Inserisci utente 
 *          2. Elenco utenti 
 *          3. Quit
 * 
 * Alla selezione di "Inserisci utente", verranno richiesti separatamente, nome e cognome dell'utente
 * ed immagazzinati in un array.
 * 
 * Alla selezione dell'elenco utenti (opzione 2), stampare una lista numerata di tutti gli utenti, es:
 * 1. Giovanni Pace
 * 2. Mario Rossi
 * 3. Valeria Verdi
 */
// let rubrica = [
//     ["GIovanni", "Pace"],
//     ["Valeria", "Verdi"],
//     ["Marika", "Viola"],
// ];

// let insAbi = true;
// while(insAbi){
//     let sceltaMenu = prompt("Scegli l'operazione:\n" + 
//         "1. Inserisci utente\n" +
//         "2. Elenco utenti\n" +
//         "3. Quit")

//     switch(sceltaMenu){
//         case "1":
//             let nome = prompt("Inserisci il nome");
//             let cognome = prompt("Inserisci il cognome");

//             // let persona = [nome, cognome];
//             let persona = [];
//             persona.push(nome);
//             persona.push(cognome);

//             rubrica.push(persona);
//             alert("Inserimento effettuato con successo!");
//             break;
//         case "2":
//             let elenco = "";
//             for(let i=0; i<rubrica.length; i++){
//                 elenco += (i+1) + ". " + rubrica[i].join(" ") + "\n";
//             }
//             console.log(elenco)
//             document.getElementById("risultato").innerHTML = elenco;
//             break;
//         case "3":                               //Caso del Quit
//             insAbi = !insAbi;
//             break;
//         default:
//             alert("Comando non riconosciuto!");
//     }
// }

// -------------------------------------------------------

let rubrica = [
    ["GIovanni", "Pace", 34],
    ["Valeria", 32, "Verdi"],
    ["Marika", "Viola", 55],
];

let somma = 0;
for(let i=0; i<rubrica.length; i++){
    somma += rubrica[i][2];
}

console.log(somma)          //Problema con gli indici


