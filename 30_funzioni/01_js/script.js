// -----------------------------------------------------
// Funzione senza parametri e ritorno

// function saluta(){
//     console.log("-------------");
//     console.log("CIAO GIOVANNI");
//     console.log("-------------");
// }

// saluta();
// console.log("Ho finito la funzione")
// saluta();
// console.log("Ho finito la funzione")
// saluta();
// console.log("Ho finito la funzione")

// ----------------------------------------------------

// function saluta(nome){
//     console.log("-------------");
//     console.log(`CIAO ${nome}`);
//     console.log("-------------");
// }

// saluta("GIOVANNI");
// saluta("ANTONIO");
// saluta("VALERIA");

// ----------------------------------------------------

// /**
//  * Funzione per la stampa in console del nome e cognome della persona (attenzione, accetta anche numeri!)
//  * @param {*} nome | Nome della persona
//  * @param {*} cognome | Cognome della persona
//  */
// function saluta(nome, cognome){
//     console.log(`Nome: ${nome}`);
//     console.log(`Cognome: ${cognome}`);
// }

// saluta("Giovanni", "Pace");
// saluta("Mario", "Rossi");

// ---------------------------------------------------
// Funzione con ritorno

// function somma(a, b){
//     let risultato = a + b;
//     return risultato;
// }

// let operazione = somma(2, 3);
// console.log(operazione);

// console.log( somma(9, 154) )

// ---------------------------------------------------
// Funzione con ritorno

// function somma(a, b){
//     if(typeof a == 'number' && typeof b == 'number'){
//         let risultato = a + b;
//         return risultato;
//     }
//     else{
//         return null;
//     }
// }

// let operazione = somma(5, 89);
// if(operazione == null)
//     console.log("Errore!")
// else
//     console.log(operazione)

// ---------------------------------------------------
// SCRIVERE UNA FUNZIONE PER DIVISIONE

// /**
//  * Metodo dedicato alla divisione di due numeri, 
//  * controlla l'effettiva diversità da zero del denominatore di frazione.
//  * @param {*} a Numeratore della frazione
//  * @param {*} b Denominatore della frazione
//  * @returns number|null
//  */
// function divisione(a, b){
//     if(typeof a == 'number' && typeof b == 'number'){
//         if(b !== 0)
//             return a/b;
//         else
//             return null;
//     }
//     else{
//         return null;
//     }
// }

// Modalità compatta, senza ramo Else
// function divisione(a, b){
//     if(typeof a == 'number' && typeof b == 'number')
//         if(b !== 0)
//             return a/b;
    
//     return null;
// }

// let risultato = divisione(20, "Giovanni");
// if(risultato != null){
//     console.log(`Il risultato è: ${risultato}`);
// }
// else{
//     console.log("Errore!")
// }

//----------------------------------------------------------
// function stampaArray(varArray){
//     console.log("\n")
//     for(let [indice, elemento] of varArray.entries()){
//         console.log((indice+1) + " - " + elemento);
//     }
// }


// let rubrica = ["Giovanni", "Mario", "Maria", "Marika"];

// stampaArray(rubrica);

// rubrica.push("Veronica")
// rubrica.push("Marco")

// stampaArray(rubrica)

//----------------------------------------------------------
/**
 * Creare un sistema di gestione di libri:
 * ogni libro sarà caratterizzato da: Titolo, Autore, ISBN
 * 
 * Nel menu di interazione avrò la possibilità di: 
 * 1. Inserire un libro                             <<
 * 2. Eliminare un libro tramite il suo ISBN        <<
 * 3. Stampare l'elenco dei libri                   <<
 * 4. Uscire dal programma
 */
function stampaArray(){
    console.log("\n")
    for(let [indice, elemento] of scaffale.entries()){
        console.log(`${indice+1}. ${elemento.titolo} - ${elemento.autore} - ${elemento.isbn}`)
    }
}

function inserisciLibro(varTitolo, varAutore, varIsbn){
    varIsbn = Number.parseInt(varIsbn);

    let libro = {
        titolo: varTitolo,
        autore: varAutore,
        isbn: varIsbn
    }

    scaffale.push(libro);
    alert(insert_ok);

    stampaArray();
}

function eliminaLibro(varIsbn){
    varIsbn = Number.parseInt(varIsbn);

    for(let [indice, elemento] of scaffale.entries()){
        if(elemento.isbn === varIsbn){
            scaffale.splice(indice, 1);
            return true;
        }
    }

    return false;
}


let scaffale = [
    {
        titolo: "La tempesta",
        autore: "W.S.",
        isbn: 123456
    },
    {
        titolo: "Lo Hobbit",
        autore: "JRRT",
        isbn: 123457
    },
    {
        titolo: "Il codice Da Vinci",
        autore: "DB",
        isbn: 123458
    },
];
let insAbi = true;

while(insAbi){

    let input = prompt(menu_iniziale);

    switch(input){
        case "1":
            let inpTitolo = prompt(ins_titolo);
            let inpAutore = prompt(ins_autore);
            let inpIsbn = prompt(ins_isbn);

            inserisciLibro(inpTitolo, inpAutore, inpIsbn);
            break;
        case "2":
            let eliIsbn = prompt(ins_isbn);

            if(eliminaLibro(eliIsbn)){
                alert("Tutto ok!")
                stampaArray();
            }
            else
                alert("Errore di eliminazione")
            break;
        case "3":
            stampaArray(scaffale);
            break;
        case "4":
            insAbi = !insAbi;
            break;
        default:
            alert(not_found);
    }

}