function findStuedenteByMatr(matr){
    for(let obj of classe){
        if(matr == obj.matricola){
            return obj;
        }
    }

    return null;
}

function inserisciStudente(){
    let varNome = document.getElementById("input-nome").value;
    let varCognome = document.getElementById("input-cognome").value;
    let varCf = document.getElementById("input-cf").value;
    let varMatricola = document.getElementById("input-matricola").value;

    if(varNome.length == 0){
        alert("Attenzione, il nome è vuoto!");
        document.getElementById("input-nome").focus();
        return;
    }    
    if(varCognome.length == 0){
        alert("Attenzione, il cognome è vuoto!");
        document.getElementById("input-cognome").focus();
        return;
    }    
    if(varCf.length == 0){
        alert("Attenzione, il Codice fiscale è vuoto!");
        document.getElementById("input-cf").focus();
        return;
    }
    if(varMatricola.length == 0){
        alert("Attenzione, la Maticola è vuota!");
        document.getElementById("input-matricola").focus();
        return;
    }

    varMatricola = Number.parseInt(varMatricola);
    if(Number.isNaN(varMatricola)){
        alert("Attenzione, la Maticola è del formato sbagliato!");
        document.getElementById("input-matricola").focus();
        return;
    }

    let studenteCercato = findStuedenteByMatr(varMatricola);
    if(studenteCercato != null){
        alert("Matricola già esistente ;(");
        document.getElementById("input-matricola").focus();
        return;
    }

    let studente = new Object({
        nome: varNome,
        cognome: varCognome,
        cf: varCf,
        matricola: varMatricola
    })

    classe.push(studente);
    localStorage.setItem("scuola", JSON.stringify(classe));
    stampaElenco();
}

function stampaScheda(valMatricola){
    let studente = findStuedenteByMatr(valMatricola);

    document.getElementById("card-nominativo-studente").innerHTML = `${studente.nome}, ${studente.cognome}`;
    document.getElementById("card-matricola-studente").innerHTML = `${studente.matricola}`;
    document.getElementById("card-cf-studente").innerHTML = `${studente.cf}`;

    document.getElementById("card-biblioteca").style.display = "";
}

function stampaElenco(){
    contenutoTabella = "";

    for(let [idx, obj] of classe.entries()){
        let stringaStudente = `<tr>`
                            +   `<td>${idx+1}</td>`
                            +   `<td>${obj.nome}</td>`
                            +   `<td>${obj.cognome}</td>`
                            +   `<td>${obj.cf}</td>`
                            +   `<td>${obj.matricola}</td>`
                            +   `<td>`
                            +       `<button type="button" class="btn btn-success" onclick="javascript: stampaScheda(${obj.matricola})"><i class="far fa-address-card"></i></button>`
                            +       `<button type="button" class="btn btn-danger ml-1" onclick="javascript: eliminaStudente(${obj.matricola})"><i class="fas fa-trash"></i></button>`
                            +       `<button type="button" class="btn btn-warning ml-1" onclick="javascript: visualizzaModaleModifcaStudente(${obj.matricola})"><i class="fas fa-pen"></i></button>`
                            +   `</td>`
                            + `</tr>`;

        contenutoTabella += stringaStudente;
    }

    document.getElementById("contenuto-tabella").innerHTML = contenutoTabella;
}

function eliminaStudente(valMatricola){
    for(let [idx, obj] of classe.entries()){
        if(obj.matricola == valMatricola){
            classe.splice(idx, 1);

            localStorage.setItem("scuola", JSON.stringify(classe))

            alert("Elemento eliminato!")
            stampaElenco();
            return;
        }
    }

    alert("Errore di eliminazione");
}

function visualizzaModaleModifcaStudente(valMatricola){
    let studente = findStuedenteByMatr(valMatricola);
    if(studente != null){
        document.getElementById("update-nome").value = studente.nome;
        document.getElementById("update-cognome").value = studente.cognome;
        document.getElementById("update-matricola").value = studente.matricola;
        document.getElementById("update-cf").value = studente.cf;

        $("#modaleModifica").modal("show");
        // console.log(studente);
    }
    else{
        alert("Errore, studente non trovato!")
    }
}

function modificaStudente(){
    let varNome = document.getElementById("update-nome").value;
    let varCognome = document.getElementById("update-cognome").value;
    let varMatricola = document.getElementById("update-matricola").value;
    let varCf = document.getElementById("update-cf").value;

    for(let [idx, obj] of classe.entries()){
        if(obj.matricola == varMatricola){
            obj.nome = varNome;
            obj.cognome = varCognome;
            obj.cf = varCf;

            localStorage.setItem("scuola", JSON.stringify(classe))

            alert("Modifica effettuata con successo!");
            $("#modaleModifica").modal("hide");
            stampaElenco();
            return;
        }
    }

    alert("Errore, studente non modificato");
}

let classe;
if(localStorage.getItem("scuola") != null)
    classe = JSON.parse(localStorage.getItem("scuola"));
else
    classe = [];

let contenutoTabella = "";
stampaElenco();

// ----------------------------------------------------------------------
/**
 * Creare un software gestionale per la manipolazione di menu:
 * Un menu è un contenitore di pietanze, ogni pietanza è caratterizzata da:
 * 
 * - Nome del piatto
 * - Descrizione del piatto
 * - Ingredienti (separati da virgola)
 * - Prezzo
 * - Link all'immagine: https://picsum.photos/
 * 
 * Creare una pagina HTML dedicata all'inserimento di un nuovo piatto!
 * Creare una pagina HTML dedicata all'elenco dei piatti con le relative funzioni di modifica ed eliminazione
 * *** Se volete *** Create un piccolo menu per passare da una pagina all'altra!
 */