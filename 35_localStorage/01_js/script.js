// SALVATAGGIO DATI
// localStorage.setItem("nominativo", "Giovanni Pace")
// localStorage.setItem("coseCheMiPiacciono", "chitarra, droni, asronomia")

// RECUPERO DI UN DATO TRAMITE CHIAVE!
// let nom = localStorage.getItem("nominativo");
// console.log(nom);

// RIMOZIONE DI UN DATO
// localStorage.removeItem("nominativo");
// localStorage.removeItem("coseCheMiPiacciono");

// AGGIORNAMENTO DI UN DATO
// localStorage.setItem("nominativo", "Giovanni Pace")
// localStorage.setItem("nominativo", "Mario Rossi")

// -------------------------------------------------------------
// Salvataggio di strutture complesse
// let persona = {
//     nome: "GIovanni",
//     cognome: "Pace",
//     eta: 35
// }

// personaJson = JSON.stringify(persona)

// // console.log(persona);
// // console.log(personaJson)

// // localStorage.setItem("gio", personaJson);

// let giovanniJson = localStorage.getItem("gio");
// let giovanni = JSON.parse(giovanniJson);
// console.log(giovanni);

// -------------------------------------------------------------
// let elenco = ["Fiat", "Lamborghini", "Maserati", "BMW"];

// localStorage.setItem("concessionaria", JSON.stringify(elenco));

let concessionariaRecuperata = JSON.parse(localStorage.getItem("concessionaria"));
console.log(concessionariaRecuperata);
