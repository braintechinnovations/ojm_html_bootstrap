function inserisciLibro(){
    let varTitolo = document.getElementById("input-titolo").value;
    let varAutore = document.getElementById("input-autore").value;
    let varAnno = document.getElementById("input-anno").value;
    let varCasa = document.getElementById("input-casa").value;
    let varIsbn = document.getElementById("input-isbn").value;

    let libro = new Object();
    libro.titolo = varTitolo;
    libro.autore = varAutore;
    libro.anno = Number.parseInt(varAnno);
    libro.casa = varCasa;
    libro.isbn = varIsbn;

    scaffale.push(libro);

    document.getElementById("input-titolo").value = "";
    document.getElementById("input-autore").value = "";
    document.getElementById("input-anno").value = "";
    document.getElementById("input-casa").value = "";
    document.getElementById("input-isbn").value = "";

    stampaElenco();
}

function stampaElenco(){
    contenutoTabella = "";

    for(let [idx, obj] of scaffale.entries()){
        let contenutoLibro  =  `<tr>`;
        contenutoLibro      += `    <td>${obj.titolo}</td>`;
        contenutoLibro      += `    <td>${obj.autore}</td>`;
        contenutoLibro      += `    <td>${obj.anno}</td>`;
        contenutoLibro      += `    <td>${obj.casa}</td>`;
        contenutoLibro      += `    <td>${obj.isbn}</td>`;
        contenutoLibro      += `</tr>`;

        contenutoTabella += contenutoLibro;
    }

    document.getElementById("contenuto-tabella").innerHTML = contenutoTabella;
}

let scaffale = [
    {
        titolo: "La tempesta",
        autore: "WS",
        anno: 1658,
        casa: "Oepli",
        isbn: "123456",
    },
    {
        titolo: "Lord of the rings",
        autore: "JRRT",
        anno: 1920,
        casa: "De Agostini",
        isbn: "123457",
    },
    {
        titolo: "Basi di dati",
        autore: "Paolo Atzeni",
        anno: 1998,
        casa: "Oepli",
        isbn: "123458",
    },
];
let contenutoTabella = "";

stampaElenco();

/**
 * Creare una tabella per la gestione di studenti, dello studente voglio tracciare:
 * Nome, Cognome, Codice Fiscale e Matricola
 * 
 * VALIDARE L'INPUT ;)
 * 
 * Tutto sarà presentato sotto forma di tabella!
 */