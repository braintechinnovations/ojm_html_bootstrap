// Prima prova di ciclo

/**
 * while(condizione){
 *      corpo della condizione in caso positivo
 * }
 */

// let numero = 6;

// while(6 > 0){
//     console.log("Ciao Giovanni")
// }

// while(true){
//     console.log("CIAO")
// }

// alert("CIAO");
// alert("Giovanni");
// alert("Pace");

// while(true){
//     alert("CIAO");      //Alert interrompe (mette in pausa) il corrente flusso di esecuzione del programma
// }

//--------------------------------------------------

//  

//-------------------------------------------------
// Scrivere un ciclo while che conta fino a 100

// let index = 93;
// let numero_massimo = 102;

// while(index < numero_massimo){
//     console.log(index);
//     index++;
// }

// console.log("FINE!")

//-------------------------------------------------
// Scrivere un ciclo while che conta fino a 0 da un numero qualsiasi (positivo)

// let numero_partenza = 15;
// let numero_destinazione = 5

// while(numero_partenza >= numero_destinazione){
//     console.log(numero_partenza);

//     numero_partenza--;
// }

//-------------------------------------------------
// Prompt per l'input di variabili
// let variabile = prompt("Ciao, inserisci il valore");

// console.log("Il valore da te inserito è: " + variabile);

//-------------------------------------------------
// Chiedere all'utente di inserire 5 numeri della lotteria fortunata e stamparli in console!

// let index = 0;
// let inserimenti = 5;

// while(index < inserimenti){
//     let variabile = prompt("Ciao, inserisci il valore");

//     console.log(variabile);
//     index++;
// }

//-------------------------------------------------
// anziché stampare volta per volta il valore inserito, stamparlo alla fine dell'inserimento come una sequenza
// di numeri separata da virgole!

// let index = 0;
// let inserimenti = 5;

// let stringa_giocata = "";

// while(index < inserimenti){
//     let variabile = prompt("Ciao, inserisci il valore");

//     stringa_giocata += variabile;

//     if(index < (inserimenti - 1)){
//         stringa_giocata += ",";
//     }

//     index++;
// }

// console.log(stringa_giocata);

//-------------------------------------------------
// Evitare inserimenti di numeri già presenti?

// let index = 0;
// let inserimenti = 5;

// let stringa_giocata = "";

// while(index < inserimenti){
//     let variabile = prompt("Ciao, inserisci il valore");

//     if(stringa_giocata.search(variabile) >= 0){
//         alert("Numero già giocato!");
//     }
//     else{
//         stringa_giocata += variabile;
//         if(index < (inserimenti - 1)){
//             stringa_giocata += ",";
//         }
        
//         index++;
//     }

// }

// console.log(stringa_giocata);

//-------------------------------------------------
// Terminatore

// let inserimento_abilitato = true;

// while(inserimento_abilitato){
//     let variabile = prompt("Ciao, inserisci il valore, digita QUIT per uscire dal programma");

//     if(variabile.search("QUIT") >= 0){                  //if(variabile.length == 0){ se voglio terminarlo inserendo nessun carattere.
//         inserimento_abilitato = !inserimento_abilitato;
//     }

//     console.log(variabile)
// }

// console.log("Programma terminato!");

//------------------------------------------------
// DO - WHILE

/**
 * do{
 *      corpo del ciclo
 * } while (condizione)
 */

// let index = 0;
// let valore_massimo = 0;

// do{
//     console.log("CIAO")

//     index++;

// } while (index < valore_massimo)

//------------------------------------------------

let inserimento_abilitato = true;

do{
    let variabile = prompt("Ciao, inserisci il valore, digita QUIT per uscire dal programma");
    console.log(variabile)

    if(variabile.search("QUIT") >= 0){
        inserimento_abilitato = !inserimento_abilitato;
    }

} while(inserimento_abilitato)