/**
 * Data una variabile con la sigla della propria provincia, mandare in output il nome completo!
 * Input: AQ
 * Output: L'Aquila
 */

// let provincia = "AN";

// IF ad espansione orizzontale
// if(provincia == "AO"){
//     console.log("Aosta")
// }
// else{
//     if(provincia == "AL"){
//         console.log("Alessandria")
//     }
//     else{
//         if(provincia == "AN"){
//             console.log("Ancona")
//         }
//         else{
//             if(provincia == "AR"){
//                 console.log("Arezzo")
//             }
//             else{
//                 if(provincia == "AQ"){
//                     console.log("L'Aquila")
//                 }
//             }
//         }
//     }
// }

// ELSE IF
// let provincia = "AO";

// if(provincia == "AO"){
//     console.log("Aosta")
// } else if(provincia == "AL"){
//     console.log("Alessandria")
// } else if(provincia == "AN"){
//     console.log("Ancona")
// } else if(provincia == "AR"){
//     console.log("Arezzo")
// } else if(provincia == "AQ"){
//     console.log("L'Aquila")
// } else {
//     console.log("Provincia non trovata")
// }

//------------------------------------------

// if(provincia == "AO"){
//     console.log("Aosta")
//     console.log("CIAO")
// }
// else
//     console.log("Provincia non trovata!")

//------------------------------------------

let provincia = "TO"

switch(provincia){
    case "AG":
        console.log("Agrigento")
        console.log("Ciao da Agrigento!")
        break;
    case "AL":
        console.log("Alessandria")
        console.log("Ciao da Alessandria!")
        break;
    case "AN":
        console.log("Ancona")
        console.log("Ciao da Ancona!")
        break; 
    case "AO":
        console.log("Aosta")
        console.log("Ciao da Aosta!")
        break;
    case "AQ":
        console.log("L'Aquila")
        console.log("Ciao da L'Aquila!")
        break;
    default:
        console.log("Provincia non trovata");
        break;
}