class Persona{
    nome;
    cognome;
    telefono;

    constructor(){
        this.nome = "N.D."
        this.cognome = "N.D."
        this.telefono = "N.D."
    }

    stampa(){
        let stringa = `${this.nome}, ${this.cognome} - Telefono: ${this.telefono}`
        console.log(stringa)
    }
}

class Studente extends Persona{
    matricola;

    stampa(){
        let stringa = `${this.nome}, ${this.cognome} - Telefono: ${this.telefono} - Matricola: ${this.matricola}`
        console.log(stringa)
    }
}

class Professore extends Persona{
    dipartimento
    materie

    stampa(){
        let stringa = `${this.nome}, ${this.cognome} - Telefono: ${this.telefono} - Dipartimento: ${this.dipartimento} - Materie: ${this.materie}`
        console.log(stringa)
    }
}

let maria = new Studente();
maria.nome = "Maria"
maria.cognome = "Viola"
maria.telefono = "123456"
maria.matricola = "AT123456"
maria.stampa();

let gio = new Persona();
gio.nome = "Giovanni"
gio.cognome = "Pace"
gio.telefono = "1231456"
// gio.eta = 34             //ERRORE, VIOLA LA OOP
gio.stampa();

let valeria = new Professore()
valeria.nome = "Valeria"
valeria.cognome = "Rossi"
valeria.dipartimento = "Fisica"
valeria.materie = "Fisica, Analisi"
valeria.stampa()