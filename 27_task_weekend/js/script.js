// TASK 1
/**
 * Scrivi un programma che prenda in input un numero intero e restituisca se è pari o dispari
Esempi:
Input: numero = 63
Output: 1
Input: numero = 24
Output: 0
 */

// let ins_abi = true;

// while(ins_abi){

//     let inserimento = prompt("Inserisci il valore oppure digita Q per uscire")
    
//     if(inserimento.search("Q") >= 0){
//         ins_abi = !ins_abi;
//     }
//     else{
//         inserimento = Number.parseInt(inserimento)
        
//         if(Number.isNaN(inserimento)){
//             alert("Attenzione, non è possibile effettuare l'operazione")
//         }
//         else{
//             let resto = inserimento % 2;
            
//             if(resto == 0)
//                 alert("PARI")
//             else
//                 alert("DISPARI")

//         }
//     }

// }


// -------------------------

// let inserimento = Number.parseInt(prompt("Inserisci il valore"))
// console.log(inserimento)

// if(Number.isNaN(inserimento)){
//     alert("Attenzione, non è possibile effettuare l'operazione")
// }
// else{
//     let resto = inserimento % 2;
//     console.log(resto);
// }

// -------------------------

// TASK 2

/**
 * Scrivi un programma che dato il seguente menu (da stampare):
    MENU:
    1. Tiramisu
    2. Torta della nonna
    3. Cheesecake alla nutella
    4. Macedonia
    Prenda in input un valore numerico che rappresenti la scelta e restituisca:
    - se la scelta non Ã¨ tra quelle elencate la scritta 'Dolce non
    disponibile',
    - altrimenti la scelta effettuata 'Hai scelto il dolce X'.

 */

// let menu = "Tiramisu, Torta della nonna, Macedonia";
// let inserimento_abilitato = true;

// while(inserimento_abilitato){
//     let scelta = prompt("MENU: \n 1. Tiramisu " + 
//     "\n 2. Torta della nonna " + 
//     "\n 3. Cheesecake alla nutella " + 
//     "\n 4. Macedonia " + 
//     "\n Q. Quit");

//     if(scelta.search("Q") >= 0){
//         inserimento_abilitato = !inserimento_abilitato;
//     }
//     else{
//         let dolce;

//         switch(scelta){
//             case "1":
//                 dolce = "Tiramisu"
//                 if(menu.search(`${dolce}`) >= 0)
//                     console.log(`Hai scelto il dolce ${dolce}`)
//                 else
//                     console.log("Dolce non disponibile")
//                 break;
//             case "2":
//                 dolce = "Torta della nonna";
//                 if(menu.search(`${dolce}`) >= 0)
//                     console.log(`Hai scelto il dolce ${dolce}`)
//                 else
//                     console.log("Dolce non disponibile")
//                 break;
//             case "3":
//                 dolce = "Cheesecake alla nutella";
//                 if(menu.search(`${dolce}`) >= 0)
//                     console.log(`Hai scelto il dolce ${dolce}`)
//                 else
//                     console.log("Dolce non disponibile")
//                 break;
//             case "4":
//                 dolce = "Macedonia";
//                 if(menu.search(`${dolce}`) >= 0)
//                     console.log(`Hai scelto il dolce ${dolce}`)
//                 else
//                     console.log("Dolce non disponibile")
//                 break;
//             default:
//                 console.log("Dolce non disponibile")
//         }
//     }
// }

// -----------------------------------------
// TASK 3
/**
 * Scrivi un programma che dato un numero stampi la tabellina corrispondente.
Esempio:
Input: 5
Output: 0 5 10 15 20 25 30 35 40 45 50
 */
// let inserimento = prompt("Inserisci il valore per scoprire la sua tabellina!");
// inserimento = Number.parseInt(inserimento);

// let stringa = "";

// if(!Number.isNaN(inserimento)){
//     for(let i=0; i<10; i++){
//         stringa += (inserimento * (i+1)) + "\n";
//     }

//     console.log(stringa)
// }
// else{
//     alert("Attenzione, valore non corretto!")
// }

//----------------------------------------------
// TASK 4
/**
 * Scrivi un programma che che stampi i numeri da 1 a 100 andando a capo ogni 10.
Esempio:
Output: 1 2 3 4 5 6 7 8 9 10
11 12 13 14 15 16 17 18 19 20
21 22 23 24 25 26 27 28 29 30
31 32 33 34 35 36 37 38 39 40
41 42 43 44 45 46 47 48 49 50
51 52 53 54 55 56 57 58 59 60
61 62 63 64 65 66 67 68 69 70
71 72 73 74 75 76 77 78 79 80
81 82 83 84 85 86 87 88 89 90
91 92 93 94 95 96 97 98 99 100
Consiglio:
Per andare a capo usa '\n'.
 */

let contenuto = "";

for(let i=1; i<=100; i++){

    if(i<10)
        contenuto += "0";

    contenuto += i + " ";
    if((i % 10) == 0)
        contenuto += "\n";
}

console.log(contenuto)

//CHALLENGE: Allineare i numeri a destra se non si parte dai canonici (con 0 finale)