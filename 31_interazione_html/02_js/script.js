function inserisciPersona(){
    let varNome = document.getElementById("input-nome").value;          //Prendo il valore della input

    if(varNome.length != 0){
        elenco +=  `<li class="list-group-item">${varNome}</li>`;           //Concateno all'elenco il nuo LI
        document.getElementById("elenco-rubrica").innerHTML = elenco;       //Inserisco nell'UL con id "elenco-rubrica" la stringa elenco
        
        document.getElementById("input-nome").value = "";                   //Pulisco la input appena utilizzata
    }
    else{
        alert("Errore, non puoi effettuare l'inserimento!")
        document.getElementById("input-nome").focus();
    }
    
}

let elenco = "";

/**
 * Creare un sistema di inserimento di libri, un libro è caratterizzato da Titolo e Autore.
 * Per il momento è possibile utilizzare la concatenazione di stringhe!
 */