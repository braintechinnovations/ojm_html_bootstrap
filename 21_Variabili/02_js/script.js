// var a = 5;
// console.log(a);
// console.log(typeof a);

//-----------------------------------------

// var b = 5.5558;
// console.log(b);
// console.log(typeof b);

//-----------------------------------------

// var c = 5e18;
// console.log(c)
// console.log(typeof c);

//-----------------------------------------

// console.log(typeof (5.69 + 96.2 / 254))

//-----------------------------------------

// var k = 5;
// var k = 8;       //Nessun errore, la var può essere ridichiarata ATTENZIONE!!!
// console.log(k);

//-----------------------------------------

// let t = 5;
// let t = 8;          //Errore, non è possibile ridichiarare una let!
// console.log(t);

//-----------------------------------------

// var numero = "89.56";
// console.log(typeof numero);

// var numero_convertito = Number.parseInt(numero);    //Output solo della parte intera (errore concettuale)
// console.log(typeof numero_convertito);
// console.log(numero_convertito)

// var numero_convertito = Number.parseFloat(numero);    //Output numero con virgola mobile
// console.log(typeof numero_convertito);
// console.log(numero_convertito)

//-----------------------------------------

// var numero = 8 / 0;     //Risultato: Infinity
// numero = -8 / 0;        //Risultato: -Infinity
// console.log(numero);

//-----------------------------------------

// var numero = " 98 anni giocatore 1"; 
// numero = Number.parseInt(numero);           //Wrapper che rimuove tutti i caratteri non consentiti
// console.log(numero);

//-----------------------------------------

// var numero  = "Ho 98 anni";
// numero = Number.parseInt(numero);
// console.log(numero);
// console.log( Number.isNaN(numero) );

//-----------------------------------------

// var valore = Infinity;
// console.log( Number.isFinite(valore) )

//-----------------------------------------

// var valore = 5.6;
// console.log( Number.isInteger(valore) )

//-----------------------------------------

// var valore = 9007199254740991;              //Vedi: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger
// var valore = 9007199254740992;
// console.log( Number.isSafeInteger(valore) )

//-----------------------------------------

// console.log( Number.MAX_SAFE_INTEGER );
// console.log( Number.POSITIVE_INFINITY );

// console.log( Number.MIN_SAFE_INTEGER );
// console.log( Number.NEGATIVE_INFINITY );

//-----------------------------------------
// let numeroUno = 5;
// numeroUno += 2;
// console.log(numeroUno);

let numeroUno = 5;
numeroUno -= 2;
console.log(numeroUno);