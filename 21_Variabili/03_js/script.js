// let nome = "Giovanni";
// let cognome = 'Pace';

// console.log(nome);
// console.log(cognome);

//-----------------------------------

// let testo = "Ciao sono Giovanni e tutti mi chiamano \"Johnny\" ";
// console.log(testo);

// let testo = 'Ciao sono Giovanni e questo è il \'Secondo\' testo';
// console.log(testo);

// let testo = "Ciao sono Giovanni equesto è il 'Terzo' testo";
// console.log(testo);

// let testo = 'Ciao sono Giovanni equesto è il "Quarto" testo';
// console.log(testo);

// let testo = "Il mondo non è tutto bianco\\nero";
// console.log(testo);

//-----------------------------------

// let testo = "Lorem ipsum dolor sit amet, " + 
//     "consectetur adipiscing elit, sed do eiusmod tempor incididunt " + 
//     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, " + 
//     "quis nostrud exercitation ullamco laboris nisi ut aliquip ex " + 
//     "ea commodo consequat. Duis aute irure dolor in reprehenderit in " + 
//     "voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

// console.log(testo);

// let testoDue = "Lorem ipsum dolor sit amet, ";
// testoDue = testoDue + "consectetur adipiscing elit, sed do eiusmod tempor incididunt ";
// testoDue += "ut labore et dolore magna aliqua. Ut enim ad minim veniam, ";

// console.log(testoDue);

// nuovo_valore = vecchio_valore + "..."

// let testo = "";
// testo += testo + "Lorem ipsum dolor sit amet, "
// testo += testo + "consectetur adipiscing elit, sed do eiusmod tempor incididunt ";
// testo += testo + "ut labore et dolore magna aliqua. Ut enim ad minim veniam, ";
// testo += testo + "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ";
// testo += testo + "ea commodo consequat. Duis aute irure dolor in reprehenderit in ";
// testo += testo + "voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

//-----------------------------------

// let nominativo = "Giovanni";
// nominativo = nominativo + " Pace";
// console.log(nominativo);

// let nominativoDue = "Francesco";
// nominativoDue += " Pace"
// console.log(nominativoDue);

//-----------------------------------
//Lunghezza di una stringa

// let nominativo = "Giovanni Pace";
// console.log("La lunghezza della stringa è: " + nominativo.length);

// let testo = "   Ciao, \"sono\" un testo!  ";
// console.log("La lunghezza della stringa è: " + testo.length);

//-----------------------------------
//Stringa sotto forma di oggetto, spoiler ;)

// let nominativo = new String("GIOVANNI");
// console.log(nominativo); 

//-----------------------------------
//Metodi di manipolazione delle stringhe

// let testo = "Supercalifragilistichespiralitoso";
// console.log( testo.substring(0,5) );
// console.log( testo.substring(7) );

// console.log( testo.substring( testo.length - 1 ) )       //Ultimo carattere

// let cod_articolo = "#12345-B42";         //#<numero_articolo>_<scaffale>
// let numero_articolo = cod_articolo.substring(1, 6);
// console.log("Numero articolo: " + numero_articolo);

// let scaffale = cod_articolo.substring(7);
// console.log("Scaffale numero: " + scaffale);

//-----------------------------------
//Metodi per effettuare un substring anche dalla fine
// let testo = "Supercalifragilistichespiralitoso";
// console.log( testo.slice(0, 5) );
// console.log( testo.slice(-3) )

//-----------------------------------
//Separazione di una stringa in tante sottostringhe a base di un delimiatatore
// let lista_spesa = "latte, biscotti, olio, carta, piatti";
// console.log( lista_spesa.split(",") );

// let lista_spesa = "latte, biscotti, olio, carta, piatti";
// // console.log( lista_spesa.replace("biscotti", "nutella") );       //Rimpiazzo i biscotti con la nutella!
// console.log( lista_spesa.replace(" ", "") );                        //Rimpiazzo il primo spazio con una stringa vuota
//                                                                     //TODO: RegEx replace

// let categorie_libro = "classico, avventura, future, fantasy, horror, thriller";
// console.log( categorie_libro.indexOf("fantasy") );

//-----------------------------------
// let categoria  = "fantasy";
// console.log( categoria.toUpperCase() )

// let categoria = "FANTASY";
// console.log( categoria.toLowerCase() )

// let cf_uno = "PCAGNN".toUpperCase();
// let cf_due = "PcaGNN".toUpperCase();
// let cf_tre = "PcAGnn".toUpperCase();

//-----------------------------------
//Inizio e fine
// let testo = "Supercalifragilistichespiralitoso";
// // console.log( testo.startsWith("Supe") )
// console.log( testo.endsWith("oso") )
                                               
// let nome_file = "cv_giovanni.docx";
// console.log( nome_file.endsWith(".pdf") )
                 
//-----------------------------------
// let codice = "AB-M-123456";                  //<settore>-<sesso>-<identificativo>
// console.log( codice.charAt(3) );             //Restituisce M

//-----------------------------------

// let nominativo = "Mario Rossi";
// let output = "Benvenuto " + nominativo + ", sei il giocatore designato!";
// console.log(output);

//La BackTick si fa con il ALT + 9 + 6 del numpad
let nominativo = "Mario Rossi";
let output = `Benvenuto "${nominativo}", sei il giocatore designato... 'divertente'!`;
console.log(output)