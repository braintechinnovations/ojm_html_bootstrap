//Elevazione a potenza
// let numero = 55;
// console.log( Math.pow(2, 6) );

//Radice Quadrata
let numero = 25;
console.log( Math.sqrt(numero) )

//-------------------------------------
//Arrotondo al numero più vicino (intero)
// let numero = 45.5;
// console.log( Math.round(numero) )

//-------------------------------------
// let numero = 45.56;
// console.log( Math.ceil(numero) )        //Parte intera arrotondata forzatamente al più grande
// console.log( Math.floor(numero) )       //Parte intera arrotondata forzatamente al più piccolo

//-------------------------------------
// let numero = -98;
// console.log( Math.abs(numero) )         //Valore assoluto

//-------------------------------------
//Minimo e massimo tra un set di numeri
// console.log( Math.min(8, 58, 1, 12.5) )
// console.log( Math.max(8, 58, 1, 12.5) )