/*
    if(condizione){
        blocco di codice in caso positivo della condizione
    } else {
        blocco di codice in caso negativo della condizione
    }
*/

// console.log(18 > 5);                //Restituisce True
// console.log(18 < 5);                //Restituisce False
// console.log(18 >= 18);                  //Restituisce True
// console.log(18 <= 18);                  //Restituisce True
// console.log(18 == 18);                      //Restituisce True
// console.log(18 === 18);                     //Restituisce True
// console.log(18 != 18);                          //Restituisce False
// console.log(18 !== 18);                          //Restituisce False
// console.log(!true);                              //Restituisce False
// console.log( !(18 == 18) )                          //Restituisce False
// console.log( !(!(18 == 18)) )                           //Come se non ci fosse!

// let variabile_booleana = true;
// console.log(!variabile_booleana);

// if(18 > 5){
//     console.log("Maggiore");
// }
// else{
//     console.log("Minore")
// }

// console.log("Inizio dell'esempio");

// if(18 > 5){
//     console.log("Maggiore")
// }

// console.log("Fine dell'esempio");

//--------------------------------------

// let eta = 18
// if(eta >= 18){
//     console.log("Sei maggiorenne!")
// }
// else{
//     console.log("Sei minorenne")
// }

//--------------------------------------

// let eta = 15;

// if(eta > 130){
//     console.log("Eh si... te li porti bene...")
// }
// else{
//     if(eta < 0){
//         console.log("Non puoi essere un embrione!")
//     }
//     else{
//         if(eta >= 18){
//             console.log("Sei maggiorenne")
//         }
//         else{
//             console.log("Sei minorenne")
//         }
//     }
// }

//--------------------------------------

//let eta = -1
//  false  &&   true          = false
//    0     x    1            = 0

// let eta = 130;
//   true  &&   false         = false
//    1    x      0           = 0

// let eta = 25;
//  true   &&    true         = true
//   1     x      1           = 1
// if(eta >= 0 && eta < 120){
//     if(eta >= 18){
//         console.log("Maggiorenne")
//     }
//     else{
//         console.log("Minorenne")
//     }
// }
// else{
//     console.log("ERRORE")
// }

//--------------------------------------

// let eta = -20;
//   true  ||   false        = true
//    1    +      0          = 1

// let eta = 250;
//  false  ||  true         = true
//    0    +     1          = 1

// let eta = 15;

// //   false ||   false      = false
// //    0     +     0        = 0     
// if(eta < 0 || eta >= 120){
//     console.log("ERRORE")
// }
// else{
//     if(eta >= 18){
//         console.log("Maggiorenne")
//     }
//     else{
//         console.log("Minorenne")
//     }
// }

//--------------------------------------
//Trasferimento del blocco di controllo
let eta = 190;
if(!(eta < 0 || eta >= 120)){
    if(eta >= 18){
        console.log("Maggiorenne")
    }
    else{
        console.log("Minorenne")
    }
}
