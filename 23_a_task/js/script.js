/* CON SWITCH-CASE
* Scrivere un piccolo programma che, dato in input il giorno della settimana (in numero), vi restituisca il nome
* - 1 = Lunedì
* - 2 = Martedì
* ...
* NON RISCRIVENDO LO SWITCH-CASE ;)
* CHALLENGE... e se volessimo cambiare il sistema di conversione dei giorni nello stile americano?
* - 1 = Domenica
* - 2 = Lunedì
* - 3 = Martedì
* ...
*
* let tipologia = "ITA";
* if(tipologia == "ENG")...
*/

// let giorno = 7;
// let tipologia = "ENG";

// if(tipologia == "ENG"){
//     giorno = giorno - 1;

//     if(giorno == 0)
//         giorno = 7;
// }

// switch(giorno){
//     case 1:
//         console.log("Lunedì")
//         break;
//     case 2:
//         console.log("Martedì")
//         break;
//     case 3:
//         console.log("Mercoledì")
//         break;
//     case 4:
//         console.log("Giovedì")
//         break;
//     case 5:
//         console.log("Venerdì")
//         break;
//     case 6:
//         console.log("Sabato")
//         break;
//     case 7:
//         console.log("Domenica")
//         break;
//     default:
//         console.log("Giorno non trovato!");
// }

//-------------------------------------------

let input = "1 - ENG";
let giorno = input.substring(0, 1);
giorno = Number.parseInt(giorno);
let tipologia = input.slice(-3);

if(tipologia == "ENG"){
    giorno = giorno - 1;

    if(giorno == 0)
        giorno = 7;
}

switch(giorno){
    case 1:
        console.log("Lunedì")
        break;
    case 2:
        console.log("Martedì")
        break;
    case 3:
        console.log("Mercoledì")
        break;
    case 4:
        console.log("Giovedì")
        break;
    case 5:
        console.log("Venerdì")
        break;
    case 6:
        console.log("Sabato")
        break;
    case 7:
        console.log("Domenica")
        break;
    default:
        console.log("Giorno non trovato!");
}